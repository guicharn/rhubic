lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rhubic/version'

Gem::Specification.new do |spec|
  spec.name      = 'rhubic'
  spec.version   = Rhubic::VERSION
  spec.authors   = ['Guillaume Charneau']
  spec.email     = ['guicharn.rhubic@gmail.com']
  spec.homepage  = 'https://gitlab.com/guicharn/rhubic'
  spec.summary   = 'SDK for Hubic'
  spec.licenses  = ['MIT']

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(spec)/}) }

  spec.add_development_dependency 'bundler'
  spec.add_development_dependency 'rspec'
  spec.add_development_dependency 'rubocop'
end
